package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

var lastBlock = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "cosmos_last_block_num",
	Help: "Last processed block in blockchain",
}, []string{"moniker"})

var timeDiff = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "cosmos_time_diff_seconds",
	Help: "Time diff of current time and time of last processed block in seconds",
}, []string{"moniker"})

var peerCount = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "cosmos_peer_count",
	Help: "Number of node peers",
}, []string{"moniker"})

func init() {
	log.SetFormatter(&logrus.JSONFormatter{})
	log.SetLevel(logrus.InfoLevel)

	prometheus.MustRegister(lastBlock)
	prometheus.MustRegister(timeDiff)
	prometheus.MustRegister(peerCount)
}
