package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

type Client struct {
	BaseURL    string
	HTTPClient *http.Client
}

func NewClient(baseUrl string) *Client {
	return &Client{
		BaseURL: baseUrl,
		HTTPClient: &http.Client{
			Timeout: 100 * time.Millisecond,
		},
	}
}

type errorResponse struct {
	Error string `json:"error"`
}

type successResponse struct {
	Result interface{} `json:"result"`
}

func (c *Client) sendRequest(req *http.Request, v interface{}) error {
	log.Debug("Sending request")

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Error)
		}

		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	fullResponse := successResponse{
		Result: v,
	}
	if err := json.NewDecoder(res.Body).Decode(&fullResponse); err != nil {
		return err
	}

	return nil
}
