package main

import (
	"fmt"
	"net/http"
	"time"
)

// Status Response
type Status struct {
	NodeInfo *NodeInfo `json:"node_info,omitempty"`
	SyncInfo *SyncInfo `json:"sync_info,omitempty"`
}

// NodeInfo defines model for NodeInfo.
type NodeInfo struct {
	Moniker *string `json:"moniker,omitempty"`
}

// SyncInfo defines model for SyncInfo.
type SyncInfo struct {
	LatestBlockHeight int       `json:"latest_block_height,string"`
	LatestBlockTime   time.Time `json:"latest_block_time,string"`
}

func (c *Client) GetStatus() (*Status, error) {
	url := fmt.Sprint(c.BaseURL, "/status")
	log.Debug(fmt.Sprintf("Getting information from: %s", url))
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	status := Status{}
	if err := c.sendRequest(req, &status); err != nil {
		return nil, err
	}

	return &status, nil
}
