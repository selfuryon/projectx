package main

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

var log = logrus.New()
var baseURL = "http://localhost:26657"

func main() {
	go UpdateMetrics()

	log.Info("Running on :8080")
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func UpdateMetrics() {
	log.Info("Running update metrics")
	c := NewClient(baseURL)
	for {

		status, err := c.GetStatus()
		if err != nil {
			log.Error("Can't get status:", err)
			time.Sleep(1000 * time.Millisecond)
			continue
		}

		netInfo, err := c.GetNetInfo()
		if err != nil {
			log.Error("Can't get net_info:", err)
			time.Sleep(1000 * time.Millisecond)
			continue
		}

		now := time.Now()
		diff := now.Sub(status.SyncInfo.LatestBlockTime) / time.Second
		timeDiff.WithLabelValues(*status.NodeInfo.Moniker).Set(float64(diff))

		peerCount.WithLabelValues(*status.NodeInfo.Moniker).Set(float64(netInfo.NPeers))
		lastBlock.WithLabelValues(*status.NodeInfo.Moniker).Set(float64(status.SyncInfo.LatestBlockHeight))

		// Wait the next round of updating
		time.Sleep(100 * time.Millisecond)
	}
}
