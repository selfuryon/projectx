package main

import (
	"fmt"
	"net/http"
)

// NetInfo Response
type NetInfo struct {
	NPeers int `json:"n_peers,string"`
}

func (c *Client) GetNetInfo() (*NetInfo, error) {
	url := fmt.Sprint(c.BaseURL, "/net_info")
	log.Debug(fmt.Sprintf("Getting information from: %s", url))
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	netInfo := NetInfo{}
	if err := c.sendRequest(req, &netInfo); err != nil {
		return nil, err
	}

	return &netInfo, nil
}
