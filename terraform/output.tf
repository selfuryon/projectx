output "public_ips" {
  value = ["${aws_instance.p2p-nodes.*.public_ip}"]
}
