# create the VPC
resource "aws_vpc" "p2p" {
  cidr_block = "10.250.0.0/16"
  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}

# create the Subnet
resource "aws_subnet" "p2p" {
  vpc_id                  = aws_vpc.p2p.id
  cidr_block              = "10.250.0.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}


# create VPC Network access control list
resource "aws_network_acl" "p2" {
  vpc_id     = aws_vpc.p2p.id
  subnet_ids = [aws_subnet.p2p.id]
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 26656
    to_port    = 26657
  }

  ingress {
    protocol   = "-1"
    rule_no    = 1000
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  # allow egress ephemeral ports
  egress {
    protocol   = "-1"
    rule_no    = 1000
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}

# Create the Internet Gateway
resource "aws_internet_gateway" "p2p" {
  vpc_id = aws_vpc.p2p.id
  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}

# Create the Route Table
resource "aws_route_table" "p2p" {
  vpc_id = aws_vpc.p2p.id
  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}

# Create the Internet Access
resource "aws_route" "p2p" {
  route_table_id         = aws_route_table.p2p.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.p2p.id
}

resource "aws_route_table_association" "p2p" {
  subnet_id      = aws_subnet.p2p.id
  route_table_id = aws_route_table.p2p.id
}
