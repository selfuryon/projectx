resource "aws_instance" "p2p-nodes" {
  count                  = 2
  ami                    = "ami-05f7491af5eef733a"
  instance_type          = "t2.micro"
  key_name               = "p2p"
  vpc_security_group_ids = [aws_security_group.cosmos.id]
  subnet_id              = aws_subnet.p2p.id
  tags = {
    Name = "p2p-nodes-${count.index + 1}"
    Org  = "p2p"
  }
}

# Create the Security Group
resource "aws_security_group" "cosmos" {
  vpc_id      = aws_vpc.p2p.id
  name        = "Cosmos SG"
  description = "SG for Cosmos Node"

  # allow ingress of port 22
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
  }

  # allow egress of all ports
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "p2p"
    Org  = "p2p"
  }
}
